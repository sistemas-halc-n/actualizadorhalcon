﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using SharpCompress.Archives;
using SharpCompress.Common;

namespace Extractor
{
    public partial class Extractor : Form
    {
        private BackgroundWorker _backgroundWorker;
        private readonly StringBuilder _stringBuilder = new StringBuilder();

        public Extractor()
        {
            InitializeComponent();
        }

        private void Extractor_Shown(object sender, EventArgs e)
        {
            _stringBuilder.Append(DateTime.Now.ToString("F"));
            _stringBuilder.AppendLine();
            _stringBuilder.Append("Extrayendo los archivos correspondientes.");

            string[] args = Environment.GetCommandLineArgs();

            for (var index = 0; index < args.Length; index++)
            {
                var arg = args[index];
                _stringBuilder.AppendLine($"[{index}] {arg}");
            }

            _stringBuilder.AppendLine();

            if (args.Length < 3) return;

            _backgroundWorker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };

            _backgroundWorker.DoWork += (o, eventArgs) =>
            {
                int bandera;

                do
                {
                    bandera = 0;

                    foreach (var process in Process.GetProcesses())
                    {
                        try
                        {
                            if (process.MainModule != null && process.MainModule.FileName.Equals(args[2]))
                            {
                                _stringBuilder.AppendLine("Esperando por el cierre de instancias");

                                _backgroundWorker.ReportProgress(0, "Cerrando otras instancias del programa...");

                                process.Kill();
                                process.WaitForExit();

                                bandera++;
                            }
                        }
                        catch (Exception exception)
                        {
                            Debug.WriteLine(exception.Message);
                        }
                    }
                } while (bandera > 0);

                _stringBuilder.AppendLine("Se incio correctamente.");

                string path = Path.GetDirectoryName(args[2]);
                string zipOrRarArchive = args[1];

                if (Path.GetExtension(zipOrRarArchive) != ".rar" && Path.GetExtension(zipOrRarArchive) != ".zip")
                {
                    _backgroundWorker.ReportProgress(0, $"Copiando el archivo {zipOrRarArchive}");
                    _stringBuilder.AppendLine("Copiando el archivo " + zipOrRarArchive + " a " + path);

                    string destFile = path + Path.DirectorySeparatorChar + Path.GetFileName(zipOrRarArchive);
                    // ReSharper disable once AssignNullToNotNullAttribute
                    if (File.Exists(destFile))
                    {
                        File.Delete(destFile);
                    }
                    File.Move(zipOrRarArchive, destFile);

                    return;
                }

                var archive = ArchiveFactory.Open(zipOrRarArchive);

                _backgroundWorker.ReportProgress(0, "Extrayendo...");

                foreach (var entry in archive.Entries)
                {
                    if (_backgroundWorker.CancellationPending)
                    {
                        eventArgs.Cancel = true;
                        return;
                    }

                    if (!entry.IsDirectory)
                    {
                        string checkForFile = path + @"\" + entry.Key;

                        do
                        {
                            bandera = 0;

                            foreach (var process in Process.GetProcessesByName(
                                Path.GetFileNameWithoutExtension(entry.Key)))
                            {
                                process.Kill();

                                bandera++;
                            }

                            foreach (var process in Process.GetProcesses())
                            {
                                try
                                {
                                    _backgroundWorker.ReportProgress(0, $"Extrayendo {entry.Key}");
                                    ProcessModuleCollection modules = process.Modules;

                                    if (process.MainModule != null && process.MainModule.FileName.Equals(checkForFile))
                                    {
                                        _backgroundWorker.ReportProgress(0,
                                            $"Cerrando el proceso que esta usando {entry.Key}");

                                        process.Kill();
                                        process.WaitForExit();

                                        bandera++;
                                    }

                                    for (int b = 0; b <= modules.Count; b++)
                                    {
                                        if (modules[b].FileName.Equals(checkForFile,
                                            StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            _backgroundWorker.ReportProgress(0,
                                                $"Cerrando el proceso que esta usando {entry.Key}");

                                            process.Kill();
                                            process.WaitForExit();

                                            bandera++;
                                        }
                                    }
                                }
                                catch (Exception exception)
                                {
                                    Debug.WriteLine(exception.Message);
                                }
                            }
                        } while (bandera > 0);

                        bool retry;
                        int retryCount = 0;

                        do
                        {
                            retry = false;

                            _stringBuilder.AppendLine("Intentado copiar " + checkForFile);

                            try
                            {
                                if (File.Exists(checkForFile))
                                {
                                    File.Delete(checkForFile);
                                    _stringBuilder.AppendLine("Se elimino la version anterior de " + checkForFile);
                                }

                                entry.WriteToDirectory(path,
                                    new ExtractionOptions() {ExtractFullPath = true, Overwrite = true});

                                _stringBuilder.AppendLine("Se copio " + checkForFile);
                            }
                            catch (Exception exception)
                            {
                                _stringBuilder.AppendLine("Hubo un problema al copiar " + checkForFile);
                                _stringBuilder.AppendLine(
                                    "***************************** ERROR *************************");
                                _stringBuilder.AppendLine(exception.ToString());
                                _stringBuilder.AppendLine(
                                    "***************************** ERROR *************************");
                                _backgroundWorker.ReportProgress(0,
                                    $"Hubo un problema extrayendo {entry.Key} reintentando");

                                retry = true;
                                retryCount++;
                            }

                            // Si hay un error escribiendo en el archivo que reintente por un maximo de diez veces
                        } while (retry && retryCount < 10);
                    }
                }
            };

            _backgroundWorker.ProgressChanged += (o, eventArgs) =>
            {
                progressBar.Value = eventArgs.ProgressPercentage;
                lblMensaje.Text = eventArgs.UserState.ToString();
            };


            _backgroundWorker.RunWorkerCompleted += (o, eventArgs) =>
            {
                try
                {
                    if (eventArgs.Error != null)
                    {
                        throw eventArgs.Error;
                    }

                    if (!eventArgs.Cancelled)
                    {
                        lblMensaje.Text = @"Finished";
                        try
                        {
                            ProcessStartInfo processStartInfo = new ProcessStartInfo(args[2]);
                            if (args.Length > 3)
                            {
                                processStartInfo.Arguments = args[3];
                            }

                            Process.Start(processStartInfo);

                            _stringBuilder.AppendLine("Successfully launched the updated application.");
                        }
                        catch (Win32Exception exception)
                        {
                            if (exception.NativeErrorCode != 1223)
                            {
                                throw;
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    _stringBuilder.AppendLine();
                    _stringBuilder.AppendLine(exception.ToString());

                    MessageBox.Show(exception.Message, exception.GetType().ToString(),
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    _stringBuilder.AppendLine();
                    Application.Exit();
                }
            };

            _backgroundWorker.RunWorkerAsync();
        }

        private void Extractor_FormClosing(object sender, FormClosingEventArgs e)
        {
            _backgroundWorker?.CancelAsync();

            _stringBuilder.AppendLine();

            File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Updater.log"),
                _stringBuilder.ToString());

            ProcessStartInfo info = new ProcessStartInfo
            {
                Arguments = "/C choice /C Y /N /D Y /T 3 & Del " +
                            Application.ExecutablePath,
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                FileName = "cmd.exe"
            };

            Process.Start(info);
        }


        private void Extractor_Load(object sender, EventArgs e)
        {
        }
    }
}
# Actualizador.

## Datos técnicos

 1. Plataforma .Net 4.6.1
 2. Lenguaje Visual C#
 4. Basado en [Auto updater](https://github.com/ravibpatel/AutoUpdater.NET)

## Prácticas usadas
|Práctica|Tipo afectado |
|-|--|
| Pascal Case, Naming Convention | Clases |
| camel Case, Naming Convention | variables, instancias |
| Documentacion de código | Métodos (publicos, protected), Propiedades (Publicas, Protected)|

## Como usar
1. Agregar la referencia ActualizadorHalcon.dll
2. Seguir los pasos de uso de [Sharpcompress](https://github.com/adamhathcock/sharpcompress)

## Otras librerías usadas.
[Sharpcompress](https://github.com/adamhathcock/sharpcompress)
Libreria para manejo de archivos .zip .rar.

 1. Instalación
	 Click derecho en capa de presentación > Administrar paquetes NuGet > Buscar [Sharpcompress](https://github.com/adamhathcock/sharpcompress) > Instalar 

## Otras configuraciones
# Configuración para assambly de el proyecto Extractor

```csharp
// Program.cs
using System;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace Extractor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomainOnAssemblyResolve;

            Application.Run(new Extractor());
            
        }

        private static Assembly CurrentDomainOnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            var currentAssembly = Assembly.GetExecutingAssembly();
            var requieredDllName = $"{(new AssemblyName(args.Name).Name)}.dll";
            var resource = currentAssembly.GetManifestResourceNames()
                .FirstOrDefault(s => s.EndsWith(requieredDllName));

            if (resource != null)
            {
                using (var stream = currentAssembly.GetManifestResourceStream(resource))
                {
                    if (stream == null) return null;

                    var block = new byte[stream.Length];
                    stream.Read(block, 0, block.Length);
                    return Assembly.Load(block);
                }
            }
            else
            {
                return null;
            }
        }
    }
}
```
# En Extractor.csproj pegar a ultima linea 
```xml
  <Target Name="AfterResolveReferences">
    <ItemGroup>
      <EmbeddedResource Include="@(ReferenceCopyLocalPaths)" Condition="'%(ReferenceCopyLocalPaths.Extension)' == '.dll'" >
      <LogicalName>%(ReferenceCopyLocalPaths.DestinationSubDirectory)%(ReferenceCopyLocalPaths.FileName)%(ReferenceCopyLocalPaths.Extension)</LogicalName>
      </EmbeddedResource>
    </ItemGroup>
  </Target>
```
# CHANGE LOG
|TAG|DESCRIPCIÓN  |
|--|--|
| FIXED | CORRECCIÓN DE PROBLEMAS O BUGS |
| ADDED | NUEVA FUNCIONALIDAD O MODULO |
| CHANGED | ACTUALIZACIÓN DE UNA FUNCIONALIDAD O MODULO |
| REMOVED | ELIMINACIÓN DE ARCHIVOS O FUNCIONALIDADES |
| SECURITY | CAMBIOS QUE IMPACTAN O MEJORAN EN LA SEGURIDAD DEL APLICATIVO |

## 11 de Mayo 2019
### Modifed
 - Se elimina referencia de DevExpress
 - Ahora el extractor.exe se crea con un nombre unico Extractor + Time.ToString para evitar posibles problemas
 - Se elimna el extractor.exe generado
 - Se agrega exception.toString al archivo log

## 15  de Octubre 2019
### ADDED

 - Se crea la libreria basada en https://github.com/ravibpatel/AutoUpdater.NET. 
 - Soporta archivos .rar
 - Mata los procesos de la app a actualizar con el fin de forzar el actualizado.

﻿using System.Windows.Forms;

namespace ActualizadorHalcon
{
    partial class UpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMensaje = new System.Windows.Forms.Label();
            this.pictureEdit1 = new System.Windows.Forms.PictureBox();
            this.progressBarControl1 = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMensaje
            // 
            this.lblMensaje.Location = new System.Drawing.Point(66, 22);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(75, 16);
            this.lblMensaje.TabIndex = 5;
            this.lblMensaje.Text = "Extrayendo...";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Image = global::ActualizadorHalcon.Properties.Resources.actualizar;
            this.pictureEdit1.Location = new System.Drawing.Point(5, 7);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Size = new System.Drawing.Size(55, 55);
            this.pictureEdit1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureEdit1.TabIndex = 4;
            this.pictureEdit1.TabStop = false;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(66, 44);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Size = new System.Drawing.Size(338, 18);
            this.progressBarControl1.TabIndex = 6;
            // 
            // UpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 79);
            this.Controls.Add(this.progressBarControl1);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.pictureEdit1);
            this.Name = "UpdateForm";
            this.Text = "UpdateForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Label lblMensaje;
        private PictureBox pictureEdit1;
        private ProgressBar progressBarControl1;
    }
}

